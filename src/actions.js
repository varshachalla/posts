/* eslint-disable */

export function addPost(data) {
  return {
    type: "ADD_POST",
    data
  };
}

export function updatePost(data) {
  return {
    type: "UPDATE_POST",
    data
  };
}

export function deletePosts(data) {
  return {
    type: "DELETE_POSTS",
    data
  };
}

export function setInitialState(data) {
  return {
    type: "SET_INIT_STATE",
    data
  };
}

