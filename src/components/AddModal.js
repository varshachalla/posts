/* eslint-disable */

import { Button,Modal,TextField,Typography,withStyles } from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {

  return {
    top: "50%",
    left: "50%",
    transform: `translate(-50%, -50%)`
  };
}

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 25,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  }
});

class AddModal extends React.Component {
  constructor() {
    super();
    this.state = {
      formData: {}
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    var targetId = event.target.id;
    var targetValue = event.target.value;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [targetId]: targetValue
      }
    }));
  }

  handleSubmit() {
    this.props.onAddClick(this.state.formData);
  }

  render() {
    const { classes,onClose, open } = this.props;
    return (
      <div>
        <Modal
          open={open}
          onClose={onClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <Typography variant="title" id="modal-title">
              Add Post
            </Typography>
            <TextField
              id="userId"
              label="userId"
              type="number"
              onChange={this.handleChange}
              margin="normal"
            />
            <br />
            <TextField
              id="title"
              label="title"
              type="text"
              onChange={this.handleChange}
              margin="normal"
            />
            <br />
            <TextField
              id="body"
              multiline
              rowsMax="4"
              label="body"
              type="text"
              onChange={this.handleChange}
              margin="normal"
            />
            <Button
              color="primary"
              onClick={() => {
                this.handleSubmit();
              }}
            >
              Submit
            </Button>
          </div>
        </Modal>
      </div>
    );
  }
}

AddModal.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(AddModal);
