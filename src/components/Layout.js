/* eslint-disable */
import {
  AppBar,
  Drawer,
  ListItem,
  List,
  Toolbar,
  Typography,
  withStyles,
} from "@material-ui/core/";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, Route, withRouter } from "react-router-dom";

import { setInitialState } from "../actions";
import PostsTable from "../containers/PostsTable";

class Layout extends Component {
  async componentDidMount() {
    await fetch("https://jsonplaceholder.typicode.com/posts")
      .then(
        result => {
          if (result.ok) return result.json();
          else throw Error(`Request rejected with status ${result.status}`);
        },
        error => {
          alert(error.message);
        }
      )
      .then(posts => this.setState({ posts }))
      .catch(error => {
        console.log(error.message);
      });
    await fetch("https://jsonplaceholder.typicode.com/users")
      .then(
        result => {
          if (result.ok) return result.json();
          else throw Error(`Request rejected with status ${result.status}`);
        },
        error => {
          alert(error.message);
        }
      )
      .then(users => this.setState({ users }))
      .catch(error => {
        console.log(error.message);
      });
    this.props.dispatch(setInitialState({ posts: this.state.posts, users: this.state.users }));
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="absolute" className={classes.appBar}>
          <Toolbar>
            <Typography variant="title" color="inherit" noWrap>
              Posts
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.toolbar} />
          <List component="nav">
            <Link to="/">
              <ListItem button>Home</ListItem>
            </Link>
            <Link to="/posts">
              <ListItem button>Posts</ListItem>
            </Link>
          </List>
        </Drawer>
        <main>
          <div className={classes.toolbar} />
          <div className={classes.body}>
            <Typography>
              <Route exact path="/" render={() => <p> Home </p>} />
              <Route exact path="/posts" component={PostsTable} />
            </Typography>
          </div>
        </main>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    height: 630,
    overflow: "hidden",
    position: "relative",
    display: "flex"
  },
  body: {
    height: 580,
    overflow: "auto",
    position: "relative",
    display: "flex"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  drawerPaper: {
    position: "relative",
    width: 240
  },
  toolbar: theme.mixins.toolbar
});

Layout.propTypes = {
  classes: PropTypes.object.isRequired
};

const styledComponent = withStyles(styles)(Layout);
export default withRouter(connect()(styledComponent));
