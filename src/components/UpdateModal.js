/* eslint-disable */

import { Button,Modal,TextField,Typography,withStyles } from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";

function getModalStyle() {
  return {
    top: "50%",
    left: "50%",
    transform: `translate(-50%, -50%)`
  };
}

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 25,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  }
});

class UpdateModal extends React.Component {
  constructor(props) {
    super();
    this.state = {
      formData: {}
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState({ formData: props.editPost });
  }

  handleChange(event) {
    var targetId = event.target.id;
    var targetValue = event.target.value;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [targetId]: targetValue
      }
    }));
  }

  handleSubmit() {
    this.props.onUpdateClick(this.state.formData);
  }

  render() {
    const { classes, open, onClose } = this.props;
    const { title, body } = this.state.formData;
    return (
      <div>
        <Modal
          open={open}
          onClose={onClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <Typography variant="title" id="modal-title">
              Update Post
            </Typography>
            <br />
            <br />
            Title
            <br />
            <TextField
              id="title"
              type="text"
              onChange={this.handleChange}
              margin="normal"
              value={title}
            />
            <br />
            <br />
            Body
            <br />
            <TextField
              id="body"
              type="text"
              onChange={this.handleChange}
              margin="normal"
              value={body}
            />
            <br />
            <br />
            <Button
              color="primary"
              onClick={() => {
                this.handleSubmit();
              }}
            >
              Submit
            </Button>
          </div>
        </Modal>
      </div>
    );
  }
}

UpdateModal.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  editPost: PropTypes.object.isRequired
};

export default withStyles(styles)(UpdateModal);
