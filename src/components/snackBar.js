/*eslint-disable*/

import { Snackbar, Fade } from "@material-ui/core";
import React, { Component } from "react";

class FadeSnackbar extends Component {
  render() {
    const { open, message } = this.props;
    return (
      <div>
        <Snackbar
          open={open}
          TransitionComponent={Fade}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{message}</span>}
        />
      </div>
    );
  }
}

export default FadeSnackbar;
