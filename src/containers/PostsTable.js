/* eslint-disable */
import {
  Button,
  Checkbox,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  Toolbar,
  Tooltip,
  Typography,
  withStyles
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import { lighten } from "@material-ui/core/styles/colorManipulator";
import classNames from "classnames";
import PropTypes from "prop-types";
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { addPost, updatePost, deletePosts } from "../actions";
import AddModal from "../components/AddModal";
import ConfirmationDialog from "../components/ConfirmationDialog";
import FadeSnackbar from "../components/snackBar";
import UpdateModal from "../components/UpdateModal";

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const columnData = [
  { id: "id", disablePadding: true, label: "Post ID" },
  { id: "title", disablePadding: false, label: "Title" },
  { id: "body", disablePadding: false, label: "Body" },
  { id: "name", disablePadding: false, label: "By" }
];

class EnhancedTableHead extends Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {columnData.map(column => {
            return (
              <TableCell
                key={column.id}
                numeric={column.numeric}
                padding={column.disablePadding ? "none" : "default"}
                sortDirection={orderBy === column.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={column.numeric ? "bottom-end" : "bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === column.id}
                    direction={order}
                    onClick={this.createSortHandler(column.id)}
                  >
                    {column.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            );
          }, this)}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  }
});

let EnhancedTableToolbar = props => {
  const { numSelected, classes, deleteDialog } = props;
  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subheading">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="title" id="tableTitle">
            Posts
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton
              aria-label="Delete"
              onClick={() => {
                deleteDialog();
              }}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <span />
        )}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020
  },
  tableWrapper: {
    overflowX: "auto"
  }
});

class EnhancedTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      order: "asc",
      orderBy: "id",
      selected: [],
      posts: [],
      users: [],
      page: 0,
      rowsPerPage: 5,
      addOpen: false,
      updateOpen: false,
      editPost: {},
      deletedMessageOpen: false,
      addedMessageOpen: false,
      updatedMessageOpen: false,
      confirmationDialogOpen: false
    };
   
    this.handleDelete = this.handleDelete.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onUpdatePost = this.onUpdatePost.bind(this);
    this.onAddPost = this.onAddPost.bind(this);
    this.deleteDialog=this.deleteDialog.bind(this);
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";
    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }
    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState(state => ({ selected: this.props.posts.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  getUserName(userId) {
    const { users } = this.props;
    var user = users.find(user => user.id == userId);
    if (user !== undefined) return user.name;
    else return userId;
  }

  onAddPost(formData) {
    this.setState({ addOpen: false });
    //formData.id = Math.floor(Date.now() / 1000);
    formData.id=Math.max.apply(Math, this.props.posts.map(function(post) { return post.id; }))+1;
    this.props.dispatch(addPost(formData));
    this.setState({ addedMessageOpen: true });
    setTimeout(() => {
      this.setState({
        addedMessageOpen: false
      });
    }, 1500);
  }

  onUpdatePost(formData) {
    this.setState({ updateOpen: false });
    this.props.dispatch(updatePost(formData));
    this.setState({ updatedMessageOpen: true });
    setTimeout(() => {
      this.setState({
        updatedMessageOpen: false
      });
    }, 1500);
  }

  deleteDialog()
  {
    const { selected } = this.state;
    if (selected.length > 0) 
    {
      this.setState({ confirmationDialogOpen: true });
    }
  }

  handleClose()
  {
    this.setState({ confirmationDialogOpen: false });
    this.setState({ selected: [] });
  }

  handleDelete() {
    this.setState({ confirmationDialogOpen: false });
    const { selected } = this.state;
      this.props.dispatch(deletePosts(selected));
      this.setState({ deletedMessageOpen: true });
      setTimeout(() => {
        this.setState({
          deletedMessageOpen: false
        });
      }, 1500);
    
    this.setState({ selected: [] });
  }

  render() {
    const { classes, posts } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, posts.length - page * rowsPerPage);
    return (
      <Fragment>
        <Button
          mini
          text-align="right"
          className="sizeSmall"
          variant="fab"
          aria-label="Add"
          color="primary"
          onClick={() => {
            this.setState({ addOpen: true });
          }}
        >
          <AddIcon />
        </Button>

        <Paper className={classes.root}>
          <EnhancedTableToolbar
            numSelected={selected.length}
            deleteDialog={this.deleteDialog}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={posts.length}
              />
              <TableBody>
                {posts
                  .sort(getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(n => {
                    const isSelected = this.state.selected.indexOf(n.id) !== -1;
                    return (
                      <TableRow
                        hover
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={n.id}
                        selected={isSelected}
                      >
                        <TableCell
                          role="checkbox"
                          onClick={event => this.handleClick(event, n.id)}
                          padding="checkbox"
                        >
                          <Checkbox checked={isSelected} />
                        </TableCell>
                        <TableCell component="th" scope="row" padding="none">
                          {n.id}
                        </TableCell>
                        <TableCell>{n.title}</TableCell>
                        <TableCell>{n.body}</TableCell>
                        <TableCell>{this.getUserName(n.userId)}</TableCell>
                        <TableCell>
                          <Button
                            mini
                            variant="fab"
                            color="primary"
                            aria-label="Edit"
                            className={classes.button}
                            onClick={() => {this.setState({ updateOpen: true, editPost: n })}}
                          >
                            Edit
                          </Button>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
          <TablePagination
            component="div"
            count={posts.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              "aria-label": "Previous Page"
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page"
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>
        <AddModal
          open={this.state.addOpen}
          onAddClick={formData => this.onAddPost(formData)}
          onClose={() => {
            this.setState({ addOpen: false });
          }}
        />
        <UpdateModal
          open={this.state.updateOpen}
          onUpdateClick={formData => this.onUpdatePost(formData)}
          onClose={() => {
            this.setState({ updateOpen: false });
          }}
          editPost={this.state.editPost}
        />
        <FadeSnackbar open={this.state.deletedMessageOpen} message="Deleted" />
        <FadeSnackbar open={this.state.addedMessageOpen} message="Post Added" />
        <FadeSnackbar
          open={this.state.updatedMessageOpen}
          message="Post Updated"
        />
        <ConfirmationDialog open={this.state.confirmationDialogOpen} handleDelete={this.handleDelete} handleClose={this.handleClose}/>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts,
    users: state.users
  };
};

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired
};

const styledComponent = withStyles(styles)(EnhancedTable);
export default connect(mapStateToProps)(styledComponent);
