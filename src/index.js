/* eslint-disable */
import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";

import "./index.css";
import Layout from "./components/Layout";
import postsFilter from "./postsFilter";
import registerServiceWorker from "./registerServiceWorker";

const store = createStore(postsFilter);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Layout />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
