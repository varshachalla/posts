/* eslint-disable */
function filterPostsReducer (state = { posts: [], users: [] }, action) {
  switch (action.type) {
    case "SET_INIT_STATE":
      return Object.assign({}, state, {
        posts: state.posts.concat([...action.data.posts]),
        users: state.users.concat([...action.data.users])
      });
    case "ADD_POST":
      return Object.assign({}, state, {
        posts: state.posts.concat([action.data])
      });
    case "UPDATE_POST": {
      console.log(action.data);
      return Object.assign({}, state, {
        posts: state.posts.map(post => {
          if (post.id == action.data.id) {
            return {
              ...post,
              title: action.data.title,
              body: action.data.body
            };
          } else return post;
        })
      });
    }
    case "DELETE_POSTS": {
      return Object.assign({}, state, {
        posts: state.posts.filter(post => action.data.indexOf(post.id) === -1)
      });
    }
    default:
      return state;
  }
};
export default filterPostsReducer;
